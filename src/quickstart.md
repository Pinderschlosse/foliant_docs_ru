# Быстрый старт

## Создание нового проекта

<pre style="background-color: #b7def5"><span>&#9888;</span> Обратите внимание, что для Docker'а используются свои собственные команды.</pre>

Для создания нового проекта Foliant введите:

    $ foliant init

Если вы используете Docker, то введите:

    $ docker run --rm -it -v `pwd`:/usr/app/src -w /usr/app/src foliant/foliant init

Далее введите название проекта (например, Hello Foliant):

    Enter the project name: Hello Foliant

В результате вы получите следующее сообщение:

     ✔ Generating Foliant project
    ─────────────────────
    Project "Hello Foliant" created in hello-foliant

Проверьте, что команда выполнилась корректно. Введите:

    $ cd hello-foliant
    $ tree

<pre style="background-color: LemonChiffon"><span>&#9888;</span><strong> Для Ubuntu:</strong> если при вводе <samp style="color: SteelBlue">tree</samp> возникает ошибка <samp style="color: DarkRed">Command 'tree' not found</samp>,  
то выполните команду <samp style="color: SteelBlue">sudo apt install tree</samp> и повторите запрос.</pre>

В итоге вы увидите локальную папку 'hello-foliant', содержащую компоненты вашего проекта:

    ├── docker-compose.yml
    ├── Dockerfile
    ├── foliant.yml
    ├── README.md
    ├── requirements.txt
    └── src
        └── index.md

где:

`foliant.yml` - файл конфигурации проекта,

`src` - директория, содержащая исходный контент проекта. По умолчанию содержит файл `index.md`,

`requirements.txt` - список пакетов Python, требуемых для проекта: бэкенды и препроцессоры, темы для MkDocs и т.д.,

`Dockerfile` и `docker-compose.yml` - файлы, необходимые для создания проекта в Docker.

## Создание и запуск локального сайта

Находясь в папке проекта, выполните команду:

    $ foliant make site

Если вы используете Docker, то введите:

    $ docker-compose run --rm hello-foliant make site

В результате вы получите следующее сообщение:

    ✔ Parsing config
    ✔ Applying preprocessor mkdocs
    ✔ Making site with MkDocs
    ─────────────────────
    Result: Hello_Foliant-<YYYY-MM-DD>.mkdocs

где:

`Hello_Foliant` - название проекта,

`<YYYY-MM-DD>` - текущая дата (генерируется автоматически). 

Сайт готов. Чтобы его просмотреть, перейдите в папку и выполните команду запуска локального HTTP-сервера:

    $ cd Hello_Foliant-<YYYY-MM-DD>.mkdocs
    $ python3 -m http.server

Вы получите сообщение:

    Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...

Локальный HTTP-сервер запущен. Перейдя по ссылке [http://0.0.0.0:8000/](http://0.0.0.0:8000/), вы увидите свой сайт.

![Landing page](https://foliant-docs.github.io/docs/images/basic-mkdocs.png)

Для остановки сайта, находясь в терминале, нажмите Ctrl+C


## Создание PDF

><span>&#9888;</span> Чтобы создать PDF, убедитесь, что у вас установлены Pandoc и TeXLive (см. раздел ["Установка"](https://pinderschlosse.gitlab.io/foliant_docs_ru/installation/))

Находясь в директории проекта, запустите:

    $ foliant make pdf



Если вы используете Docker, то:

- раскомментируйте строку `foliant/foliant:pandoc` в `Dockerfile` вашего проекта. В итоге вы должны получить следующее:

        # FROM foliant/foliant
        # If you plan to bake PDFs, uncomment this line and comment the line above:
        FROM foliant/foliant:pandoc
        
        COPY requirements.txt .
        
        RUN pip3 install -r requirements.txt

    >Eсли вы ранее запускали `docker-compose run` со старым образом, то запустите `docker-compose build`, чтобы перестроить образ. Кроме того, запускайте его всякий раз, когда вам нужно обновить версии необходимых пакетов из `requirements.txt`.

- Далее выполните команду, находясь в директории проекта:

        $ docker-compose run --rm hello-foliant make pdf

Вы получите следующее сообщение:

    ✔ Parsing config
    ✔ Applying preprocessor flatten
    ✔ Making pdf with Pandoc
    ─────────────────────
    Result: Hello_Foliant-<YYYY-MM-DD>.pdf 

где:

`Hello_Foliant` - название проекта,

`<YYYY-MM-DD>` - текущая дата (генерируется автоматически).

Ваш PDF-файл готов! Он будет выглядеть примерно так:

![PDF](https://foliant-docs.github.io/docs/images/basic-pdf.png)



