# Решение проблем

---

#### При установке на Ubuntu возникает ошибка (13: Permission denied) :

    E: Could not open lock file /var/lib/dpkg/lock-frontend - open (13: Permission denied)
    E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), are you root?

Такая ошибка возникает, если параллельно выполняется еще один установщик, либо предыдущая работа установщика не была завершена корректно. Есть 3 варианта решения:

**Вариант 1**

Выполните команду:
 
    sudo killall apt apt-get

Будут завершены **все** процессы `apt` и `apt-get` сразу.

**Вариант 2**

Выполните команду:

    ps aux | grep -i apt

Вы получите похожее сообщение:

    root 5183 0.0 0.0 79516 3752 pts/1 S+ 10:31 0:00 sudo apt install -y python3 python3-pip
    usernam+ 9456 0.0 0.0 38892 944 pts/0 S+ 10:32 0:00 grep --color=auto -i apt

Нам нужны PID (идентификаторы процессов) из всех строк, кроме последней. В нашем случае это `5183`.

Принудительно завершите процесс:

    sudo kill -9 5183


**Вариант 3**
        
Если предыдущие варианты вам не помогли, либо у вас нет запущенных процессов `apt` и `apt-get`, то выполните следующие команды:

    sudo rm /var/lib/apt/lists/lock
    sudo rm /var/cache/apt/archives/lock
    sudo rm /var/lib/dpkg/lock
    sudo rm /var/lib/dpkg/lock-frontend
    sudo dpkg --configure -a

<pre style="background-color: #b7def5">При выполнении могут появиться сообщения о невозможности удаления или отсутствии  
файлов. Просто игнорируйте их.</pre>

После выполнения этих действий проблема должна решиться.

---

#### В браузере вместо сайта виден список из папок и файлов.

Убедитесь, что перед запуском вы перешли в папку сайта. Она имеет расширение **.mkdocs**. Также убедитесь, что вы предварительно [создали сайт](https://pinderschlosse.gitlab.io/foliant_docs_ru/quickstart/#_3).
