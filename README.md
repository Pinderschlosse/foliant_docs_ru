#   [Foliant](https://github.com/foliant-docs/foliant). Документация на русском языке.

На данный момент переведены и переработаны следующие разделы:

- О Foliant'е
- Установка
- Быстрый старт
- Changelog

Находятся в работе:

- How to...
- Backends

Регулярно пополняются:

- Решение проблем

В работе использованы следующие источники:

Документация Foliant на английском языке: [https://foliant-docs.github.io/docs/](https://foliant-docs.github.io/docs/);

Гайд "Быстрый вход в Foliant": [https://norrskog.gitlab.io/foliant-fast-intro/](https://norrskog.gitlab.io/foliant-fast-intro/)
